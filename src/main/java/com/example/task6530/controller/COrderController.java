package com.example.task6530.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.task6530.model.COrder;
import com.example.task6530.repository.ICOrderRepository;

@RestController
@RequestMapping("/v1/order")
@CrossOrigin(value="*", maxAge = -1)
public class COrderController {

    @Autowired
    ICOrderRepository uIOrderRepository;

    @Autowired
    ICOrderRepository uIcOrderRepository;

    @GetMapping("/all")
    public ResponseEntity<Object> getAllOrder(){
        List<COrder> orderList = new ArrayList<>();
        try {
            uIOrderRepository.findAll().forEach(orderList::add);
            return new ResponseEntity<Object>(orderList, HttpStatus.OK);
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<Object>(null, HttpStatus.NOT_FOUND);
        }

    }
    // @GetMapping("/search")
    // public ResponseEntity<Object> getOrderByUserId(@RequestParam(name="id", required = true)long id){
    //     List<COrder> _orderFound = uIcOrderRepository.findBycUserId();
    //     if(!_orderFound.isEmpty()){
    //         return new ResponseEntity<Object>(_orderFound, HttpStatus.OK);
    //     } else {
    //         return new ResponseEntity<Object>(null, HttpStatus.NOT_FOUND);
    //     }
    // }

    @GetMapping("/detail/{id}")

    public ResponseEntity<Object> getUserById(@RequestParam(name = "id", required = true) long id) {
        Optional<COrder> _orderFound = uIcOrderRepository.findById(id);
        if (_orderFound.isPresent()) {
            return new ResponseEntity<Object>(_orderFound, HttpStatus.OK);
        } else {
            return new ResponseEntity<Object>(null, HttpStatus.NOT_FOUND);
        }
    }
    @PostMapping("/create")
    public ResponseEntity<Object> createOrder(@RequestBody COrder orderFromClient) {
        try {
            COrder _order = new COrder(orderFromClient.getOrderCode(), orderFromClient.getPizzaSize(), orderFromClient.getPizzaType(), orderFromClient.getVoucherCode(), orderFromClient.getPrice(), orderFromClient.getPaid());
            Date _now = new Date();
            _order.setCreated(_now);
            _order.setUpdated(null);
            uIcOrderRepository.save(_order);
            return new ResponseEntity<Object>(_order, HttpStatus.OK);
        } catch (Exception e) {
            // TODO: handle exception
            return ResponseEntity.unprocessableEntity()
                    .body("Fail to create specified user:" + e.getCause().getCause().getMessage());
        }
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<Object> updateOrder(@PathVariable(name = "id") Long id, @RequestBody COrder orderUpdate) {
        Optional<COrder> _orderData = uIcOrderRepository.findById(id);
        if (_orderData.isPresent()) {
            COrder _order = _orderData.get();
            if (orderUpdate.getOrderCode() != null && !orderUpdate.getOrderCode().isEmpty()) {
                _order.setOrderCode(orderUpdate.getOrderCode());

            }
            if (orderUpdate.getPizzaSize() != null && !orderUpdate.getPizzaSize().isEmpty()) {
                _order.setPizzaSize(orderUpdate.getPizzaSize());

            }
            if (orderUpdate.getPizzaType() != null && !orderUpdate.getPizzaType().isEmpty()) {
                _order.setPizzaType(orderUpdate.getPizzaType());

            }
            if (orderUpdate.getVoucherCode() != null && !orderUpdate.getVoucherCode().isEmpty()) {
                _order.setVoucherCode(orderUpdate.getVoucherCode());

            }
            _order.setUpdated(new Date());

            try {
                return ResponseEntity.ok(uIcOrderRepository.save(_order));
            } catch (Exception e) {
                // TODO: handle exception
                return ResponseEntity.unprocessableEntity()
                        .body("Can't execute operation of this Entity" + e.getCause().getCause().getMessage());
            }
        } else {
            return new ResponseEntity<Object>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Object> deleteOrder(@PathVariable("id") Long id) {
        Optional<COrder> _orderData = uIcOrderRepository.findById(id);
        if (_orderData.isPresent()) {
            try {
                uIcOrderRepository.deleteById(id);
                return new ResponseEntity<Object>(HttpStatus.NO_CONTENT);

            } catch (Exception e) {
                // TODO: handle exception
                return ResponseEntity.unprocessableEntity()
                .body("Can not execute operation of this Entity" + e.getCause().getCause().getMessage());
            }

        } else {
            return new ResponseEntity<Object>(null, HttpStatus.NOT_FOUND);
        }
    }
    
}
