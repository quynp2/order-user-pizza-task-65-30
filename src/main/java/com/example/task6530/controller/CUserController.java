package com.example.task6530.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.task6530.model.CUser;
import com.example.task6530.repository.IUserRepository;

@RestController
@CrossOrigin(value = "*", maxAge = -1)
@RequestMapping("/v1/user")
public class CUserController {

    @Autowired
    IUserRepository uIUserRepository;

    @GetMapping("/all")
    public ResponseEntity<Object> getAllUser() {
        List<CUser> userList = new ArrayList<CUser>();
        uIUserRepository.findAll().forEach(userList::add);
        if (!userList.isEmpty()) {
            return new ResponseEntity<Object>(userList, HttpStatus.OK);
        } else {
            return new ResponseEntity<Object>(null, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/detail")

    public ResponseEntity<Object> getUserById(@RequestParam(name = "id", required = true) long id) {
        Optional<CUser> userFound = uIUserRepository.findById(id);
        if (userFound.isPresent()) {
            return new ResponseEntity<Object>(userFound, HttpStatus.OK);
        } else {
            return new ResponseEntity<Object>(null, HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/create")
    public ResponseEntity<Object> createUser(@RequestBody CUser userFromClient) {
        try {
            CUser _user = new CUser(userFromClient.getFullname(), userFromClient.getEmail(), userFromClient.getPhone(),
                    userFromClient.getAddress());
            Date _now = new Date();
            _user.setCreated(_now);
            _user.setUpdated(null);
            uIUserRepository.save(_user);
            return new ResponseEntity<Object>(_user, HttpStatus.OK);
        } catch (Exception e) {
            // TODO: handle exception
            return ResponseEntity.unprocessableEntity()
                    .body("Fail to create specified user:" + e.getCause().getCause().getMessage());
        }
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<Object> updateUser(@PathVariable(name = "id") Long id, @RequestBody CUser userUpdate) {
        Optional<CUser> _userData = uIUserRepository.findById(id);
        if (_userData.isPresent()) {
            CUser _user = _userData.get();
            if (userUpdate.getFullname() != null && !userUpdate.getFullname().isEmpty()) {
                _user.setFullname(userUpdate.getFullname());

            }
            if (userUpdate.getEmail() != null && !userUpdate.getEmail().isEmpty()) {
                _user.setEmail(userUpdate.getEmail());

            }
            if (userUpdate.getPhone() != null && !userUpdate.getPhone().isEmpty()) {
                _user.setPhone(userUpdate.getPhone());

            }
            if (userUpdate.getAddress() != null && !userUpdate.getAddress().isEmpty()) {
                _user.setAddress(userUpdate.getAddress());

            }
            _user.setUpdated(new Date());

            try {
                return ResponseEntity.ok(uIUserRepository.save(_user));
            } catch (Exception e) {
                // TODO: handle exception
                return ResponseEntity.unprocessableEntity()
                        .body("Can't execute operation of this Entity" + e.getCause().getCause().getMessage());
            }
        } else {
            return new ResponseEntity<Object>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Object> deleteUser(@PathVariable("id") Long id) {
        Optional<CUser> _userData = uIUserRepository.findById(id);
        if (_userData.isPresent()) {
            try {
                uIUserRepository.deleteById(id);
                return new ResponseEntity<Object>(HttpStatus.NO_CONTENT);

            } catch (Exception e) {
                // TODO: handle exception
                return ResponseEntity.unprocessableEntity()
                .body("Can not execute operation of this Entity" + e.getCause().getCause().getMessage());
            }

        } else {
            return new ResponseEntity<Object>(null, HttpStatus.NOT_FOUND);
        }
    }

}
