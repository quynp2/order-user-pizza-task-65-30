package com.example.task6530.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name = "pizza_orders")
public class COrder {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name="order_code", nullable=false, updatable=false)
    private String orderCode;
    
    @Column(name="pizza_size", nullable=false, updatable=true)
    private String pizzaSize;

    @Column(name="pizza_type", nullable=false, updatable=true)
    private String pizzaType;

    @Column(name="voucher_code", nullable=true, updatable=true)
    private String voucherCode;

    @Column(name="price", nullable=true, updatable=true)
    private long price;

    @Column(name="paid", nullable=true, updatable=true)
    private long paid;

    @Temporal(TemporalType.TIMESTAMP)
    @CreatedDate
    @Column(name="created_at", nullable = true, updatable = false)
    private Date created;

    @Temporal(TemporalType.TIMESTAMP)
    @LastModifiedDate
    @Column(name="updated_at", nullable = true, updatable = true)
    private Date updated;

    @ManyToOne
    @JoinColumn(name="user_id", referencedColumnName = "id")
    @JsonBackReference
    private CUser cUser;

    /**
     * 
     */
    public COrder() {
        super();
    }

    /**
     * @param orderCode
     * @param pizzaSize
     * @param pizzaType
     * @param voucherCode
     * @param price
     * @param paid
     */
    public COrder(String orderCode, String pizzaSize, String pizzaType, String voucherCode, long price, long paid) {
        this.orderCode = orderCode;
        this.pizzaSize = pizzaSize;
        this.pizzaType = pizzaType;
        this.voucherCode = voucherCode;
        this.price = price;
        this.paid = paid;
    }

    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * @return the orderCode
     */
    public String getOrderCode() {
        return orderCode;
    }

    /**
     * @param orderCode the orderCode to set
     */
    public void setOrderCode(String orderCode) {
        this.orderCode = orderCode;
    }

    /**
     * @return the pizzaSize
     */
    public String getPizzaSize() {
        return pizzaSize;
    }

    /**
     * @param pizzaSize the pizzaSize to set
     */
    public void setPizzaSize(String pizzaSize) {
        this.pizzaSize = pizzaSize;
    }

    /**
     * @return the pizzaType
     */
    public String getPizzaType() {
        return pizzaType;
    }

    /**
     * @param pizzaType the pizzaType to set
     */
    public void setPizzaType(String pizzaType) {
        this.pizzaType = pizzaType;
    }

    /**
     * @return the voucherCode
     */
    public String getVoucherCode() {
        return voucherCode;
    }

    /**
     * @param voucherCode the voucherCode to set
     */
    public void setVoucherCode(String voucherCode) {
        this.voucherCode = voucherCode;
    }

    /**
     * @return the price
     */
    public long getPrice() {
        return price;
    }

    /**
     * @param price the price to set
     */
    public void setPrice(long price) {
        this.price = price;
    }

    /**
     * @return the paid
     */
    public long getPaid() {
        return paid;
    }

    /**
     * @param paid the paid to set
     */
    public void setPaid(long paid) {
        this.paid = paid;
    }

    /**
     * @return the created
     */
    public Date getCreated() {
        return created;
    }

    /**
     * @param created the created to set
     */
    public void setCreated(Date created) {
        this.created = created;
    }

    /**
     * @return the updated
     */
    public Date getUpdated() {
        return updated;
    }

    /**
     * @param updated the updated to set
     */
    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    /**
     * @return the cUser
     */
    public CUser getcUser() {
        return cUser;
    }

    /**
     * @param cUser the cUser to set
     */
    public void setcUser(CUser cUser) {
        this.cUser = cUser;
    }

    



    

    
    



    


    
}
