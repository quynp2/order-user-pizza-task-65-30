package com.example.task6530.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.example.task6530.model.COrder;
import com.example.task6530.model.CUser;

public interface ICOrderRepository extends CrudRepository<COrder, Long>{
    List<COrder> findBycUserId(CUser cUser);
} 
