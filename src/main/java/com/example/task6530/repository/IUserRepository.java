package com.example.task6530.repository;

import org.springframework.data.repository.CrudRepository;

import com.example.task6530.model.CUser;

public interface IUserRepository extends CrudRepository<CUser, Long> {
    
}
