package com.example.task6530;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Task6530Application {

	public static void main(String[] args) {
		SpringApplication.run(Task6530Application.class, args);
	}

}
